from controller import Controller
from collector import DataCollector
import json

with open("rooms.json") as f:
    rooms = json.load(f)

sensors = []
topic2room = {}

for room_name, room in rooms.items():
    for device in room["devices"]:
        if device['type'] in ['pir']:
            sensors.append(device['topic'])
            topic2room[device['topic']] = room_name

with open("credentials.json") as f:
    cred = json.load(f)
    host = cred['host']
    port = 1883
    if "port" in cred:
        port = cred["port"]
    upw = None
    if "username" in cred and "password" in cred:
        upw = (cred['username'], cred['password'])

ctrl = Controller(topic2room)
dc = DataCollector(ctrl, host, sensors, port, upw)