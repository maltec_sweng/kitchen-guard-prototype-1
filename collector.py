"""
Data collector module

Receive data from MQTT and send relevant events.
When a PIR updates its occupancy value, send an event to the controller
"""

from paho.mqtt.client import Client as MqttClient, MQTTMessage
from paho.mqtt import subscribe
from controller import Controller
import json

class DataCollector:
    last_data: dict
    controller: Controller

    # Constructor
    def __init__(self, controller:Controller, mqtt_host:str, topics:list, mqtt_port:int=1883, username_pw:tuple[str,str] = None) -> None:
        self.last_data = {}
        self.controller = controller
        self.client = MqttClient()
        self.client.on_message = self.on_message
        self.client.on_connect = self.on_connect
        if not username_pw is None:
            self.client.username_pw_set(*username_pw)
        self.client.connect(mqtt_host, mqtt_port)
        for t in topics:
            self.client.subscribe(t)
        
        self.client.loop_forever()
    
    # Function for estimating success of connection
    def on_connect(self, client, userdata, flags, rc):              # rc = return code
        if rc==0:
            print("Connected OK Returned code = ", rc)
        else:
            print("Bad connection Returned code = ", rc)

    # Function for updating the data collector when a new sensor reading is collected
    def on_message(self, client, userdata, message: MQTTMessage):
        #self.controller.update(new_data)
        payload = json.loads(message.payload.decode("utf-8"))
        
        try:
            last = self.last_data[message.topic]
        except KeyError:
            last = None
        
        current = payload['occupancy']

        # If no new data is documented, the system will not react.
        if last != current:
            self.controller.update(
                (message.topic, current)
            )
            self.last_data[message.topic] = current
