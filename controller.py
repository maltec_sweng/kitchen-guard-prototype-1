"""
Central system controller module.

In this prototype it should just print/log the location of the user based on occupation events from the collector.
"""

class Controller:
    def __init__(self, t2r) -> None:
        self.topic2room = t2r
        self.rooms = dict()
    
    def update(self, data):
        # Event called by the data collector when there is new data
        topic, occupancy = data
        room = self.topic2room[topic]
        #print(f"{topic = }, {occupancy = }, {room = }")

        action = 'entered' if occupancy else 'left'
        print(f"The user {action} the room '{room}'")

        # Store occupancy level for each room
        self.rooms[room] = occupancy

        # Print values for each room
        for r, occ in self.rooms.items():
            print(f"Room {r}: ", end="")
            if occ:
                print("Occupied")
            else:
                print("Empty")
        print("\n")


